import React from 'react';
import './App.css';
import Button from "./Button";
import Modal from "./Modal";

class App extends React.Component {

    state = {
        firstModalVisible: false,
        secondModalVisible: false,
    }

    toggleFirstModal = () => {
        this.setState((prevState) => ({
            firstModalVisible: !prevState.firstModalVisible,
        }));
    };
    toggleSecondModal = () => {
        this.setState((prevState) => ({
            secondModalVisible: !prevState.secondModalVisible,
        }));
    };

    render() {
        return (
            <div className="App">
                <Button
                    backgroundColor='#4040ff'
                    text='Open first modal'
                    onClick={this.toggleFirstModal}
                />
                <Button
                    backgroundColor='#19e619'
                    text='Open second modal'
                    onClick={this.toggleSecondModal}
                />

                {
                    this.state.firstModalVisible && (
                        <Modal
                            header='Do you want to delete this file?'
                            closeButton={true}
                            text='Once you delete this file, it won&#39;t be possible to undo this action. Are you sure you want to delete it?'
                            onClick={this.toggleFirstModal}
                            actions={[
                                <Button backgroundColor='#62d44d'
                                        text='Оk'
                                        onClick={this.toggleFirstModal}
                                        key='ok'
                                />,
                                <Button backgroundColor='#cc0000'
                                        text='Cencel'
                                        onClick={this.toggleFirstModal}
                                        key='cencel'
                                />
                            ]
                            }
                            backgroundColor='#ff7b5a'
                        />
                    )
                }

                {
                    this.state.secondModalVisible && (
                        <Modal
                            header='Do you want to create this file?'
                            closeButton={true}
                            text='Are you sure you want to create this file?'
                            onClick={this.toggleSecondModal}
                            actions={[
                                <Button backgroundColor='#62d44d'
                                        text='Оk'
                                        onClick={this.toggleSecondModal}
                                        key='ok'
                                />,
                                <Button backgroundColor='#cc0000'
                                        text='Cencel'
                                        onClick={this.toggleSecondModal}
                                        key='cencel'
                                />
                            ]
                            }
                            backgroundColor='#90ff77'
                        />
                    )
                }
            </div>
        )
    }
}


export default App;
