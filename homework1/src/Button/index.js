import React from 'react';

export default class Button extends React.PureComponent {
    render () {
        return (
            <button className='btn' style={{backgroundColor:this.props.backgroundColor}} onClick={this.props.onClick}>{this.props.text}</button>
        )
    }
}