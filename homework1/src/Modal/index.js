import React from 'react';

export default class Modal extends React.PureComponent {
    componentDidMount() {
        const modal = document.querySelector('.modal-overlay')
        modal.addEventListener('click', this.closeModal)
    }
    closeModal = (event) => {
       if (event.target.classList.contains('modal-overlay') || event.target.classList.contains('btn__close-modal')) {
           this.props.onClick();
       }
    }
    render() {
        return (
            <div className='modal-overlay'>
                <div className='modal' style={{backgroundColor:this.props.backgroundColor}}>
                    <h2>{this.props.header}</h2>
                    { this.props.closeButton &&
                        <span className='btn__close-modal' onClick={this.props.onClick}>&times;</span>
                    }
                    <p>{this.props.text}</p>
                    <div className="modal-actions">{this.props.actions}</div>
                </div>
            </div>
        );
    }
}