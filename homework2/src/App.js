import React from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";
import ProductsList from "./components/ProductsList";
import Header from "./components/Header";


class App extends React.Component {

    state = {
        firstModalVisible: false,
        secondModalVisible: false,
        cartCount: 0,
        favorCount: 0
    }

    toggleFirstModal = () => {
        this.setState((prevState) => ({
            firstModalVisible: !prevState.firstModalVisible,
        }));
    };
    toggleSecondModal = () => {
        this.setState((prevState) => ({
            secondModalVisible: !prevState.secondModalVisible,
        }));
    };

    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('productsInCart'));
        const favoritesItems = JSON.parse(localStorage.getItem('productsInFavor'));
        this.setState({cartCount: cart.length, favorCount: favoritesItems.length})
    }

    setCartAmount = () => {
        const cartItems = JSON.parse(localStorage.getItem('productsInCart'));
        this.setState({
            cartCount: cartItems.length
    })
    }

    setFavorAmount = () => {
        const favoritesItems = JSON.parse(localStorage.getItem("productsInFavor"));
        this.setState({
            favorCount: favoritesItems.length
        })
    }

    render() {
        return (
            <div className="App">
                <Header favorCount={this.state.favorCount} cartCount={this.state.cartCount}></Header>

                <ProductsList
                    setCartAmount={this.setCartAmount}
                    setFavorAmount={this.setFavorAmount}
                    onClick={this.toggleSecondModal.bind(this)}
                    modalVisible={this.state.secondModalVisible}>
                </ProductsList>
            </div>

        )
    }
}

export default App;
