import React from 'react';
import PropTypes from "prop-types";


class Button extends React.PureComponent {
    render () {
        return (
            <button className='btn' style={{backgroundColor:this.props.backgroundColor}} onClick={this.props.onClick}>{this.props.text}</button>
        )
    }
}

Button.defaultProps = {
    backgroundColor: '#ffffff'
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string
}


export default Button