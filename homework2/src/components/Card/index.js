import React from 'react';
import Icon from "../img";
import '../Modal/scss/modal.scss'
import PropTypes from "prop-types";


class Card extends React.PureComponent {

    render() {
        return (
            <>
                <div className="products__item">
                    <img src={'/img/' + this.props.product.url} alt="card" className="products__img"/>

                    {this.props.product.isFavorite ?
                        <span className="icon-favorites" onClick={() => this.props.removeFromFavorites(this.props.product.article)}>
                        <Icon color="gold"/>
                        </span> :
                        <span className="icon-favorites" onClick={() => this.props.addToFavorites(this.props.product.article)}>
                         <Icon color="#bdbebd"/>
                        </span>
                    }
                    <div className="products__description">
                        <h3 className="products__title">{this.props.product.title}</h3>
                        <p className="products__info">
                            <span className="products__price">Ціна: {this.props.product.price} грн</span>
                            <span className="products__info--item">Артикул: {this.props.product.article}</span>
                            <span className="products__info--item">Колір: {this.props.product.color}</span>
                        </p>
                        <button className="btn__add" onClick={() => this.props.toggleVisibleModal(this.props.product.article)}>Add to cart</button>
                    </div>
                </div>

            </>
        )
    }
}
Card.propTypes = {
    product: PropTypes.object.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    removeFromFavorites: PropTypes.func.isRequired,
    addCartProduct: PropTypes.func.isRequired,
    removeFromCart: PropTypes.func.isRequired,
    toggleVisibleModal: PropTypes.func.isRequired,
    modalVisible: PropTypes.bool.isRequired,
}
export default Card