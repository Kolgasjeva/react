import React from 'react';
import Cart from '../imgCard'
import './scss/header.scss'
import Icon from "../img";
import PropTypes from "prop-types";

class Header extends React.PureComponent {
    render() {
        return (
            <header className="header">
                <Icon color="gold"/>
                {this.props.favorCount !== 0 && <span>{this.props.favorCount}</span>}
                <span ><Cart/></span>
                {this.props.cartCount !== 0 && <span>{this.props.cartCount}</span>}
            </header>
        );
    }
}

Header.propTypes = {
    favorCount: PropTypes.number.isRequired,
    cartCount: PropTypes.number.isRequired,

}

export default Header