import React from 'react';
import './scss/modal.scss'
import PropTypes from "prop-types";

 class Modal extends React.PureComponent {
    componentDidMount() {
        const modal = document.querySelector('.modal-overlay')
        modal.addEventListener('click', this.closeModal)
    }
    closeModal = (event) => {
       if (event.target.classList.contains('modal-overlay') || event.target.classList.contains('btn__close-modal')) {
           this.props.onClick();
       }
    }
    render() {
        return (
            <div className='modal-overlay'>
                <div className='modal' style={{backgroundColor:this.props.backgroundColor}}>
                    <h2>{this.props.header}</h2>
                    { this.props.closeButton &&
                        <span className='btn__close-modal' onClick={this.props.onClick}>&times;</span>
                    }
                    <p>{this.props.text}</p>
                    <div className="modal-actions">{this.props.actions}</div>
                </div>
            </div>
        );
    }
}
Modal.defaultProps = {
    closeButton: true,
    backgroundColor: "#ffffff"
};

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    actions: PropTypes.object.isRequired,
    closeButton: 2,
    backgroundColor: PropTypes.string,
}


export default Modal