import React from 'react';
import Card from "../Card";
import './scss/productsList.scss'
import Modal from "../Modal";
import Button from "../Button";
import PropTypes from "prop-types";

class ProductsList extends React.PureComponent {

    state = {
        products: [],
        productsInCart: 0,
        productsInFavor: 0,
        modalVisible: false,
        appropriateArticle: -1,
    }

    getList() {
        fetch('/products.json')
            .then(response => response.json())
            .then(result => {
                const favoriteStateArray = JSON.parse(localStorage.getItem("productsInFavor"));
                const cartArray = JSON.parse(localStorage.getItem("productsInCart"));
                const updatedResult = result.map(item => {
                    const favoriteItem = favoriteStateArray.find(favoriteItem => favoriteItem.article === item.article);
                    const isFavorite = favoriteItem ? favoriteItem.isFavorite : false;
                    const cartItem = cartArray.find(cartItem => cartItem.article === item.article);
                    const inCart = cartItem ? cartItem.inCart : false;
                    return {...item, isFavorite, inCart};
                })


                console.log(updatedResult)
                this.setState({
                    products: updatedResult,
                });
            })

    }

    setSelectedProducts() {
        const cart = JSON.parse(localStorage.getItem("productsInCart"));
        if (!cart) {
            localStorage.setItem("productsInCart", JSON.stringify([]));
            this.setState({ productsInCart: 0 });
        } else {
            this.setState({ productsInCart: cart.length });
        }

        const favor = JSON.parse(localStorage.getItem("productsInFavor"));
        if (!favor) {
            localStorage.setItem("productsInFavor", JSON.stringify([]));
            this.setState({ productsInFavor: 0 });
        } else {
            this.setState({ productsInFavor: favor.length });
        }
    }
    componentDidMount() {
        this.getList()
        this.setSelectedProducts()
    }

    addToFavorites = (article) => {
        const favoritesItem = JSON.parse(localStorage.getItem("productsInFavor"));
        const list = [...this.state.products]
        let item = null
        for (let i = 0; i < list.length; i++) {
            const element = list[i]
            if (element.article === article) {
                item = element;
                item.isFavorite = true
                favoritesItem.push(item)
                localStorage.setItem("productsInFavor", JSON.stringify(favoritesItem))
                break
            }
        }
        if (!item) return
        this.setState({
            products : list
        })
        this.props.setFavorAmount()
    }


    removeFromFavorites = (article) => {
        const favoritesItem = JSON.parse(localStorage.getItem("productsInFavor"));
        const list = [...this.state.products]
        for (let i = 0; i < list.length; i++) {
            const element = list[i];
            if (element.article === article) {
                element.isFavorite = false;
                break;
            }
        }
        const updatedFavoritesItem = favoritesItem.filter(item => item.article !== article);
        localStorage.setItem("productsInFavor", JSON.stringify(updatedFavoritesItem));

        this.setState({
            products: list
        });
        this.props.setFavorAmount()
    }

    addCartProduct = (article) => {
        console.log(`addCartProduct`, article)

        const cartItems = JSON.parse(localStorage.getItem("productsInCart"));
        const list = [...this.state.products]
        let item = null
        for (let i = 0; i < list.length; i++) {
            const element = list[i]
            if (element.article === article) {
                item = element;
                item.inCart = true
                cartItems.push(item)
                localStorage.setItem("productsInCart", JSON.stringify(cartItems))
                break
            }
        }
        if (!item) return
        this.setState({
            products : list
        })
        this.props.setCartAmount()
        this.toggleVisibleModal(-1);
    }

    toggleVisibleModal = (article) => {
        if (!article) {
            article = -1;
        }

        this.setState(() => ({
            appropriateArticle: article,
            modalVisible: article !== -1,
        }));
    };
    removeFromCart = (article) => {
        const cartItems = JSON.parse(localStorage.getItem("productsInCart"));
        const list = [...this.state.products];
        for (let i = 0; i < list.length; i++) {
            const element = list[i];
            if (element.article === article) {
                element.inCart = false;
                break;
            }
        }
        const updatedCartItems = cartItems.filter(item => item.article !== article);
        localStorage.setItem("productsInCart", JSON.stringify(updatedCartItems));

        this.setState({
            products: list
        });
        this.props.setCartAmount()
    }


    render() {
        return (
            <>
                <div className="products__list">
                    {
                        this.state.products.map(product => (
                            <Card key={product.article}
                                  product={product}
                                  addToFavorites={this.addToFavorites.bind(this)}
                                  removeFromFavorites ={this.removeFromFavorites.bind(this)}
                                  addCartProduct = {this.addCartProduct.bind(this)}
                                  removeFromCart = {this.removeFromCart.bind(this)}
                                  toggleVisibleModal={this.toggleVisibleModal.bind(this)}
                                  modalVisible={this.state.modalVisible}
                            ></Card>
                        ))
                    }
                </div>

                { this.state.modalVisible &&
                    <Modal
                        header='Confirm adding'
                        closeButton={true}
                        text='Do you want to add a product to your cart?'
                        onClick={() => this.toggleVisibleModal(-1)}
                        actions={[
                            <Button
                                backgroundColor='#62d44d'
                                text='Оk'
                                onClick={()=>this.addCartProduct(this.state.appropriateArticle)}
                                key='ok'
                            />,
                            <Button
                                backgroundColor='#cc0000'
                                text='Cancel'
                                onClick={() => this.toggleVisibleModal(-1)}
                                key='cancel'
                            />
                        ]}
                        backgroundColor="#90ff77"
                    />
                }
            </>

        )
    }
}

ProductsList.propTypes = {
    setCartAmount: PropTypes.func.isRequired,
    setFavorAmount:PropTypes.func.isRequired,
    onClick:PropTypes.func.isRequired,
    modalVisible:PropTypes.bool.isRequired
}
export default ProductsList

