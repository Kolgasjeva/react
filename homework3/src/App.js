import React, {useState, useEffect} from 'react';
import './App.css';
import ProductsList from "./components/ProductsList";


import Header from "./components/Header";
import ProductsPages from "./pages/ProductsPages";
import FavorProductsPages from "./pages/FavorProductPages";
import CartPages from "./pages/CartPages"
import {BrowserRouter, useRoutes, Navigate} from "react-router-dom";



function Routes() {

    const [firstModalVisible, setFirstModalVisible] = useState(false);
    const [secondModalVisible, setSecondModalVisible] = useState(false);
    const [cartCount, setCartCount] = useState(0);
    const [favorCount, setFavorCount] = useState(0);

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem('productsInCart'));
        const favoritesItems = JSON.parse(localStorage.getItem('productsInFavor'));
        setCartCount(cart.length);
        setFavorCount(favoritesItems.length)
    })
    const toggleSecondModal = () => {
        setSecondModalVisible(!secondModalVisible);
    };
    const setCartAmount = () => {
        const cartItems = JSON.parse(localStorage.getItem('productsInCart'));
        setCartCount(cartItems.length);
    }

    const setFavorAmount = () => {
        const favoritesItems = JSON.parse(localStorage.getItem("productsInFavor"));
        setFavorCount(favoritesItems.length)
    }

    return (
        useRoutes([
            {
                path: '/',
                element: <Header favorCount={favorCount} cartCount={cartCount}/>,
                children: [
                    {
                        path: '',
                        element: <Navigate to="productlist/"/>,
                    },
                    {
                        path: 'productlist/',
                        element: <ProductsPages
                            setCartAmount={setCartAmount}
                            setFavorAmount={setFavorAmount}
                            modalVisible={secondModalVisible}/>


                    },
                    {
                        path: 'productlist/favor/',
                        element: <FavorProductsPages
                            setCartAmount={setCartAmount}
                            setFavorAmount={setFavorAmount}
                            modalVisible={secondModalVisible}/>
                    },
                    {
                        path: 'productlist/cart/',
                        element: <CartPages
                            setCartAmount={setCartAmount}
                            setFavorAmount={setFavorAmount}
                            modalVisible={secondModalVisible}/>
                    },

                ]
            }
        ])
    )
}

export default function App() {
    return (
        <>
            <BrowserRouter>
                <Routes/>
            </BrowserRouter>
        </>
    )
}

