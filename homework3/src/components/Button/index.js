import React from 'react';
import PropTypes from "prop-types";


function Button(props) {
    return (
        <button className='btn' style={{backgroundColor: props.backgroundColor}}
                onClick={props.onClick}>{props.text}</button>
    )
}

Button.defaultProps = {
    backgroundColor: '#ffffff'
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string
}


export default Button