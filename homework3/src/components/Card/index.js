import React from 'react';
import Icon from "../img";
import '../Modal/scss/modal.scss'
import PropTypes from "prop-types";


function Card(props) {
    return (
        <>
            <div className="products__item">
                <img src={'/img/' + props.product.url} alt="card" className="products__img"/>

                {props.product.isFavorite ?
                    <span className="icon-favorites" onClick={() => props.removeFromFavorites(props.product.article)}>
                        <Icon color="gold"/>
                        </span> :
                    <span className="icon-favorites" onClick={() => props.addToFavorites(props.product.article)}>
                         <Icon color="#bdbebd"/>
                        </span>
                }
                <div className="products__description">
                    <h3 className="products__title">{props.product.title}</h3>
                    <p className="products__info">
                        <span className="products__price">Ціна: {props.product.price} грн</span>
                        <span className="products__info--item">Артикул: {props.product.article}</span>
                        <span className="products__info--item">Колір: {props.product.color}</span>
                    </p>
                    {props.product.inCart ?
                        <>
                        <button className="btn__remove btn__close-modal" onClick={() => props.toggleVisibleModalRemove(props.product.article)}>&times;</button>
                        <p class="status__in-cart">Item in cart</p>
                        </> :
                        <button className="btn__add" onClick={() => props.toggleVisibleModal(props.product.article)}>Add to cart
                        </button>}
                </div>
            </div>

        </>
    )
}

Card.propTypes = {
    product: PropTypes.object.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    removeFromFavorites: PropTypes.func.isRequired,
    toggleVisibleModal: PropTypes.func.isRequired,
}
export default Card