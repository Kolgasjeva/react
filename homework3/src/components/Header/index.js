import React from 'react';
import Cart from '../imgCard'
import './scss/header.scss'
import Icon from "../img";
import PropTypes from "prop-types";
import { NavLink, Outlet } from "react-router-dom";


function Header(props) {
    return (
        <>
        <header className="header">
        <NavLink className="navbar__link" to="/productlist/">
            Home
        </NavLink>
        <NavLink className="navbar__link"  to="/productlist/favor/">
            <Icon color="gold"/>
            {props.favorCount !== 0 && <span className="navbar__count">{props.favorCount}</span>}
        </NavLink>
        <NavLink className="navbar__link" to="/productlist/cart/">
            <span><Cart/></span>
            {props.cartCount !== 0 && <span className="navbar__count">{props.cartCount}</span>}
        </NavLink>
        </header>
    <main>
        <Outlet></Outlet>
    </main>
        </>
    )
}

Header.propTypes = {
    favorCount: PropTypes.number.isRequired, cartCount: PropTypes.number.isRequired,

}

export default Header