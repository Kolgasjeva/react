import React, {useEffect} from 'react';
import './scss/modal.scss'
import PropTypes from "prop-types";

function Modal(props) {

    useEffect(() => {
        const modal = document.querySelector('.modal-overlay')
        modal.addEventListener('click', closeModal)
    })
    const closeModal = (event) => {
        if (event.target.classList.contains('modal-overlay') || event.target.classList.contains('btn__close-modal')) {
            props.onClick();
        }
    }
    return (
        <div className='modal-overlay'>
            <div className='modal' style={{backgroundColor: props.backgroundColor}}>
                <h2>{props.header}</h2>
                {props.closeButton &&
                    <span className='btn__close-modal' onClick={props.onClick}>&times;</span>
                }
                <p>{props.text}</p>
                <div className="modal-actions">{props.actions}</div>
            </div>
        </div>
    );
}

Modal.defaultProps = {
    closeButton: true,
    backgroundColor: "#ffffff"
};

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    actions: PropTypes.object.isRequired,
    closeButton: PropTypes.bool,
    backgroundColor: PropTypes.string,
}


export default Modal