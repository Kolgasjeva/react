import React, {useState, useEffect} from 'react';
import Card from "../Card";
import './scss/productsList.scss'
import Modal from "../Modal";
import Button from "../Button";
import PropTypes from "prop-types";

function ProductsList(props ) {

    const [products, setProducts] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [modalVisibleRemove, setVisibleModalRemove] = useState(false);
    const [appropriateArticle, setAppropriateArticle] = useState(-1);
    console.log(props.products)


    const addToFavorites = (article) => {
        const favoritesItem = JSON.parse(localStorage.getItem("productsInFavor"))
        const list = [...props.products]
        console.log(list)
        let item = null
        for (let i = 0; i < list.length; i++) {
            const element = list[i]
            if (element.article === article) {
                item = element;
                item.isFavorite = true
                favoritesItem.push(item)
                localStorage.setItem("productsInFavor", JSON.stringify(favoritesItem))
                break
            }
        }

        console.log(list)

        props.setFavorAmount()
    }


    const removeFromFavorites = (article) => {
        const favoritesItem = JSON.parse(localStorage.getItem("productsInFavor"));
        const list = [...props.products]
        for (let i = 0; i < list.length; i++) {
            const element = list[i];
            if (element.article === article) {
                element.isFavorite = false;
                break;
            }
        }
        const updatedFavoritesItem = favoritesItem.filter(item => item.article !== article);
        localStorage.setItem("productsInFavor", JSON.stringify(updatedFavoritesItem));

        props.setFavorAmount()
        if(props.inPageFavor) {
            props.setSelectedProducts()
        }
    }

    const addCartProduct = (article) => {

        const cartItems = JSON.parse(localStorage.getItem("productsInCart"));
        const list = [...props.products]
        let item = null
        for (let i = 0; i < list.length; i++) {
            const element = list[i]
            if (element.article === article) {
                item = element;
                item.inCart = true
                cartItems.push(item)
                localStorage.setItem("productsInCart", JSON.stringify(cartItems))
                break
            }
        }
        if (!item) return

        props.setCartAmount()
        toggleVisibleModal(-1);
    }

    const toggleVisibleModal = (article) => {
        if (!article) {
            article = -1;
        }

        setAppropriateArticle(article)
        setModalVisible(article !== -1)
    };

    const toggleVisibleModalRemove = (article) => {
        if (!article) {
            article = -1;
        }

        setAppropriateArticle(article)
        setVisibleModalRemove(article !== -1)
    };
    const removeFromCart = (article) => {
        const cartItems = JSON.parse(localStorage.getItem("productsInCart"));
        const list = [...props.products];
        for (let i = 0; i < list.length; i++) {
            const element = list[i];
            if (element.article === article) {
                element.inCart = false;
                break;
            }
        }
        const updatedCartItems = cartItems.filter(item => item.article !== article);
        localStorage.setItem("productsInCart", JSON.stringify(updatedCartItems));
        props.setCartAmount()
        toggleVisibleModalRemove(-1)
        if(props.inPageCart) {
            props.setSelectedProducts()
        }
    }
    return (

        <>
            <div className="products__list">
                {
                    props.products.map(product => (
                        <Card key={product.article}
                              product={product}
                              addToFavorites={addToFavorites}
                              removeFromFavorites={removeFromFavorites}
                              toggleVisibleModal={toggleVisibleModal}
                              toggleVisibleModalRemove={toggleVisibleModalRemove}>
                        ></Card>
                    ))
                }
            </div>

            {modalVisible &&
                <Modal
                    header='Confirm adding'
                    closeButton={true}
                    text='Do you want to add a product to your cart?'
                    onClick={() => toggleVisibleModal(-1)}
                    actions={[
                        <Button
                            backgroundColor='#62d44d'
                            text='Оk'
                            onClick={() => addCartProduct(appropriateArticle)}
                            key='ok'
                        />,
                        <Button
                            backgroundColor='#cc0000'
                            text='Cancel'
                            onClick={() => toggleVisibleModal(-1)}
                            key='cancel'
                        />
                    ]}
                    backgroundColor="#90ff77"
                />
            }
            {modalVisibleRemove &&
                <Modal
                    header='Confirm removing'
                    closeButton={true}
                    text='Do you want to remove a product from your cart?'
                    onClick={() => toggleVisibleModalRemove(-1)}
                    actions={[
                        <Button
                            backgroundColor='#62d44d'
                            text='Оk'
                            onClick={() => removeFromCart(appropriateArticle)}
                            key='ok'
                        />,
                        <Button
                            backgroundColor='#cc0000'
                            text='Cancel'
                            onClick={() => toggleVisibleModalRemove(-1)}
                            key='cancel'
                        />
                    ]}
                    backgroundColor="#ff7b5a"
                />
            }
        </>

    )
}

ProductsList.propTypes = {
    setCartAmount: PropTypes.func.isRequired,
    setFavorAmount: PropTypes.func.isRequired,
    modalVisible: PropTypes.bool
}
export default ProductsList

