import {useEffect, useState} from "react";
import ProductsList from "../../components/ProductsList";

export default function CartPages(props) {
    const [productsInCartArr, setProductsInCartArr] = useState([]);
    const [productsInFavor, setProductsInFavor] = useState(0);
    const [productsInCart, setProductsInCart] = useState(0);
    const [inPageFavor, setInPageFavor] = useState(false);
    const [inPageCart, setInPageCart] = useState(true)

    function setSelectedProducts() {
        const cart = JSON.parse(localStorage.getItem("productsInCart"));
        if (!cart) {
            localStorage.setItem("productsInCart", JSON.stringify([]));
            setProductsInCart(0)
        } else {
            setProductsInCart(cart.length)

        }

        const favor = JSON.parse(localStorage.getItem("productsInFavor"));
        if (!favor) {
            localStorage.setItem("productsInFavor", JSON.stringify([]));
            setProductsInFavor(0)
        } else {
            setProductsInFavor(favor.length)
        }

        setProductsInCartArr(cart)
    }

    useEffect(() => {
            setSelectedProducts();
        }, []
    )
    return (
        <ProductsList
            products={productsInCartArr}
            setCartAmount={props.setCartAmount}
            setFavorAmount={props.setFavorAmount}
            modalVisible={props.secondModalVisible}
            setSelectedProducts={setSelectedProducts}
            inPageFavor={inPageFavor}
            inPageCart={inPageCart}>

        </ProductsList>
    )
}