import {useEffect, useState} from "react";
import ProductsList from "../../components/ProductsList";

export default function FavorProductsPages(props) {

    const [productsInFavorArr, setProductsInFavorArr] = useState([]);
    const [productsInFavor, setProductsInFavor] = useState(0);
    const [productsInCart, setProductsInCart] = useState(0);
    const [inPageFavor, setInPageFavor] = useState(true);
    const [inPageCart, setInPageCart] = useState(false);

    function setSelectedProducts() {
        const cart = JSON.parse(localStorage.getItem("productsInCart"));
        if (!cart) {
            localStorage.setItem("productsInCart", JSON.stringify([]));
            setProductsInCart(0)
        } else {
            setProductsInCart(cart.length)

        }

        const favor = JSON.parse(localStorage.getItem("productsInFavor"));
        if (!favor) {
            localStorage.setItem("productsInFavor", JSON.stringify([]));
            setProductsInFavor(0)
        } else {
            setProductsInFavor(favor.length)
            setProductsInFavorArr(favor)
            console.log(favor)
        }
    }

    useEffect(() => {
            setSelectedProducts();
        }, []
    )
    return (
        <ProductsList
            products={productsInFavorArr}
            setCartAmount={props.setCartAmount}
            setFavorAmount={props.setFavorAmount}
            modalVisible={props.secondModalVisible}
            setSelectedProducts={setSelectedProducts}
            inPageFavor={inPageFavor}
            inPageCart={inPageCart}>
        </ProductsList>
    )
}