import {useEffect, useState} from "react";
import ProductsList from "../../components/ProductsList";

export default function ProductsPages(props) {
    const [products, setProducts] = useState([]);
    const [productsInCart, setProductsInCart] = useState(0);
    const [productsInFavor, setProductsInFavor] = useState(0);
    const [inPageFavor, setInPageFavor] = useState(false);
    const [inPageCart, setInPageCart] = useState(false);

    function getList() {
        fetch('/products.json')
            .then(response => response.json())
            .then(result => {
                const favoriteStateArray = JSON.parse(localStorage.getItem("productsInFavor"));
                const cartArray = JSON.parse(localStorage.getItem("productsInCart"));
                const updatedResult = result.map(item => {
                    const favoriteItem = favoriteStateArray.find(favoriteItem => favoriteItem.article === item.article);
                    const isFavorite = favoriteItem ? favoriteItem.isFavorite : false;
                    const cartItem = cartArray.find(cartItem => cartItem.article === item.article);
                    const inCart = cartItem ? cartItem.inCart : false;
                    return {...item, isFavorite, inCart};
                })
                setProducts(updatedResult)
            })
    }

    function setSelectedProducts() {
        const cart = JSON.parse(localStorage.getItem("productsInCart"));
        if (!cart) {
            localStorage.setItem("productsInCart", JSON.stringify([]));
            setProductsInCart(0)
        } else {
            props.setCartAmount(cart.length)
            setProductsInCart(cart.length)
        }

        const favor = JSON.parse(localStorage.getItem("productsInFavor"));
        if (!favor) {
            localStorage.setItem("productsInFavor", JSON.stringify([]));
            setProductsInFavor(0)
        } else {
            setProductsInFavor(favor.length)
        }
    }

    useEffect(() => {
            getList();
            setSelectedProducts();
        },[]
    )
    return (
        <ProductsList
            products={products}
            setCartAmount={props.setCartAmount}
            setFavorAmount={props.setFavorAmount}
            modalVisible={props.secondModalVisible}
            setSelectedProducts={setSelectedProducts}
            inPageFavor={inPageFavor}
            inPageCart={inPageCart}>
        </ProductsList>
    )
}