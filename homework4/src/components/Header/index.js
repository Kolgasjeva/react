import React from 'react';
import Cart from '../imgCard'
import './scss/header.scss'
import Icon from "../img";
import { NavLink, Outlet } from "react-router-dom";
import * as productsReducer from "../../store/reducers/productsReducer";
import {useSelector} from "react-redux";


function Header() {
    const favorites = useSelector(state => state.productsReducer.favoriteProducts)
    const cart = useSelector(state => state.productsReducer.cartProducts)

    return (
        <>
            <header className="header">
                <NavLink className="navbar__link" to="/productlist/">
                    Home
                </NavLink>
                <NavLink className="navbar__link"  to="/productlist/favor/">
                    <Icon color="gold"/>
                    {favorites.length !== 0 && <span className="navbar__count">{favorites.length}</span>}
                </NavLink>
                <NavLink className="navbar__link" to="/productlist/cart/">
                    <span><Cart/></span>
                    {cart.length !== 0 && <span className="navbar__count">{cart.length}</span>}
                </NavLink>
            </header>
            <main>
                <Outlet></Outlet>
            </main>
        </>
    )
}

export default Header