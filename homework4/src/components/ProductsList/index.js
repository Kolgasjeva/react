import Card from "../Card";
import './scss/productsList.scss'
import Modal from "../Modal";
import Button from "../Button";
import {useSelector, useDispatch} from "react-redux";

import * as modalReducer from "../../store/reducers/modalReducer";
import * as productsReducer from "../../store/reducers/productsReducer";

function ProductsList(props ) {
    const addToCartModal = useSelector(state => state.modalReducer.addToCartModal);
    const removeFromCartModal = useSelector(state => state.modalReducer.removeFromCartModal);

    const dispatch = useDispatch();


    const addCartProduct = (article) => {
        dispatch(productsReducer.addToCart({article: article}))
        hideAddToCartModal()
    }

    const hideAddToCartModal = () => {
        dispatch(modalReducer.toggleAddToCartModal({article: -1}))
    }

    const hideRemoveFromCartModal = () => {
        dispatch(modalReducer.toggleRemoveFromCartModal({article: -1}))
    }

    const removeFromCart = (article) => {
        dispatch(productsReducer.removeFromCart({article: article}))
        hideRemoveFromCartModal()
    }

    return (
        <>
            <div className="products__list">
                {
                    props.products.map(product => (
                        <Card
                            key={product.article}
                            product={product}
                        ></Card>
                    ))
                }
            </div>

            {addToCartModal !== -1 &&
                <Modal
                    header='Confirm adding'
                    closeButton={true}
                    text='Do you want to add a product to your cart?'
                    onClick={() => hideAddToCartModal()}
                    actions={[
                        <Button
                            backgroundColor='#62d44d'
                            text='Оk'
                            onClick={() => addCartProduct(addToCartModal)}
                            key='ok'
                        />,
                        <Button
                            backgroundColor='#cc0000'
                            text='Cancel'
                            onClick={() => hideAddToCartModal()}
                            key='cancel'
                        />
                    ]}
                    backgroundColor="#90ff77"
                />
            }
            {removeFromCartModal !== -1 &&
                <Modal
                    header='Confirm removing'
                    closeButton={true}
                    text='Do you want to remove a product from your cart?'
                    onClick={() => hideRemoveFromCartModal(-1)}
                    actions={[
                        <Button
                            backgroundColor='#62d44d'
                            text='Оk'
                            onClick={() => removeFromCart(removeFromCartModal)}
                            key='ok'
                        />,
                        <Button
                            backgroundColor='#cc0000'
                            text='Cancel'
                            onClick={() => hideRemoveFromCartModal(-1)}
                            key='cancel'
                        />
                    ]}
                    backgroundColor="#ff7b5a"
                />
            }
        </>
    )
}

export default ProductsList

