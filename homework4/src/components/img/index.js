import React from 'react';

export default function Icon ({color}) {
    return (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" width="40" height="40">
        <polygon
            points="50,5 61.18,32.31 90.76,35.16 67.88,55.09 76.47,82.09 50,68.18 23.53,82.09 32.12,55.09 9.24,35.16 38.82,32.31"
            fill={color} />
    </svg>)
}