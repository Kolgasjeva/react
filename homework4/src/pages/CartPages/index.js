import {useSelector} from "react-redux";
import ProductsList from "../../components/ProductsList";


export default function CartPages(props) {
    const cart = useSelector(state => state.productsReducer.cartProducts)
    const products = useSelector(state => state.productsReducer.products)

    function getProductsInCart() {
        return products.filter(product => cart.includes(product.article))
    }

    return (
        <>
            { cart.length === 0 ? <div className="no-items">No items in cart :(</div> : <ProductsList products={getProductsInCart()}></ProductsList> }
        </>
    )
}