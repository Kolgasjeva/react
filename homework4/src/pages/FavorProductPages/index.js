import ProductsList from "../../components/ProductsList";
import {useSelector} from "react-redux";

import * as productsReducer from "../../store/reducers/productsReducer";

export default function FavorProductsPages() {
    const favorites = useSelector(state => state.productsReducer.favoriteProducts)
    const products = useSelector(state => state.productsReducer.products)

    function getFavoriteProducts() {
        return products.filter(product => favorites.includes(product.article))
    }

    return (
        <ProductsList
            products={getFavoriteProducts()}>
        </ProductsList>
    )
}