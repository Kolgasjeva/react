

import {Provider} from "react-redux";
import {configureStore, combineReducers} from "@reduxjs/toolkit";
import thunk from "redux-thunk";

import modalReducer from "./reducers/modalReducer";
import productsReducer from "./reducers/productsReducer"

import storage from 'redux-persist/lib/storage'
import {persistReducer, persistStore} from "redux-persist";
import {PersistGate} from "redux-persist/integration/react";





const allReducers = combineReducers({
    modalReducer,
    productsReducer,
});

const persistedReducers = persistReducer({key: 'root', storage}, allReducers);

const store = configureStore({
    reducer: persistedReducers,
    middleware: [thunk],
});


const persistedStore = persistStore(store);


export default function Store(props) {
    return (
        <>
            <Provider store={store}>
                <PersistGate persistor={persistedStore}>
                    {props.children}
                </PersistGate>
            </Provider>
        </>
    )
}