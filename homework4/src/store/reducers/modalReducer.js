import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    addToCartModal: -1,
    removeFromCartModal: -1,
};

const modalReducer = createSlice({
    name: 'modalReducer',
    initialState,
    reducers: {
        toggleAddToCartModal(state, action) {
            state.addToCartModal = action && action.payload && action.payload.article || -1;
        },
        toggleRemoveFromCartModal(state, action) {
            state.removeFromCartModal = action && action.payload && action.payload.article || -1;
        },
    }
})
export const {toggleAddToCartModal,toggleRemoveFromCartModal } = modalReducer.actions
export default modalReducer.reducer