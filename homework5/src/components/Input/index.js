export default function Input(props) {
    const {field, form, label, ...rest} = props;
    const {name} = field;

    return (
        <div className="mt-2">
            <label >
                <input {...field} {...rest} className="form-control"/>
                {form.errors[name] && form.touched[name] &&
                    <div className='alert alert-danger'>{form.errors[name]}</div>}
            </label>
        </div>
    )
}