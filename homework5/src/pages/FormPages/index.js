import {Field, Form, Formik} from "formik";
import * as Yup from 'yup';

import Input from "../../components/Input";
import {useSelector, useDispatch} from "react-redux";
import * as userInfoReducer from "../../store/reducers/userInfoReducer";
import {Navigate} from "react-router-dom";
import React from "react";

export default function FormPages() {
    const userInfoStatus = useSelector(state => state.userInfoReducer.userInfoStatus)
    const dispatch = useDispatch()
    if (userInfoStatus) {
        return (<Navigate to="/productlist/userinfo/"/>)
    }
    const handleSubmit = (userInfo) => {
        const {firstname, lastname, age, deliveryAddress, phoneNumber} = userInfo;
        console.log(userInfo)
        dispatch(userInfoReducer.addUserInfo({userInfo}));
    }

    return (
        <Formik
            initialValues={{firstname: '', lastname: '', age: '', deliveryAddress: '', phoneNumber: ''}}
            onSubmit={handleSubmit}
            validationSchema={Yup.object({
                firstname: Yup.string().max(15, 'Must be 15 characters or less').required('firstname is required').matches(/^[^\p{P}\p{S}\d]+$/u, 'Invalid firstname format'),
                lastname: Yup.string().max(20, 'Must be 20 characters or less').required('lastname is required').matches(/^[^\p{P}\p{S}\d]+$/u,'Invalid firstname format'),
                age: Yup.number().integer('Age must be an integer').min(0, 'Age must be at least 0').required('Age is required'),
                deliveryAddress: Yup.string().required('Delivery address is required'),
                phoneNumber: Yup.string().matches(/^\d{10}$/, 'Phone number must be 10 digits').required('Phone number is required'),
            })}
        >
            <Form className='form__user-address' noValidate>
                <Field
                    type='text'
                    placeholder='firstname'
                    name='firstname'
                    component={Input}
                />
                <Field
                    type='text'
                    placeholder='lastname'
                    name='lastname'
                    component={Input}
                />
                <Field
                    type='numder'
                    placeholder='age'
                    name='age'
                    component={Input}
                />
                <Field
                    type='text'
                    placeholder='delivery address'
                    name='deliveryAddress'
                    component={Input}
                />
                <Field
                    type='tel'
                    placeholder='phone number'
                    name='phoneNumber'
                    component={Input}
                />
                <div className="mt-2">
                    <button type="submit" className="btn btn-primary">Login in</button>
                </div>
            </ Form>
        </Formik>
    )
}




