import {useEffect} from "react";
import ProductsList from "../../components/ProductsList";
import {useDispatch, useSelector} from "react-redux";
import {fetchAsync} from "../../store/reducers/productsReducer";

export default function ProductsPages(props) {
    const products = useSelector(state => state.productsReducer.products);
    const dispatch = useDispatch();

    function getList() {
        dispatch(fetchAsync())
    }

    useEffect(() => {
            getList();
        },[]
    )
    return (
        <ProductsList products={products}></ProductsList>
    )
}