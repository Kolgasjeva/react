import React from "react";
import * as userInfoReducer from "../../store/reducers/userInfoReducer";
import {useSelector, useDispatch} from "react-redux";
import {Navigate} from "react-router-dom";


export default function UserInfoPages() {
    const userInfoStatus = useSelector(state => state.userInfoReducer.userInfoStatus);
    const userInfoObject = useSelector(state => state.userInfoReducer.userInfoObject);
    const dispatch = useDispatch();
    if (!userInfoStatus) {
        return (<Navigate to="/productlist/form/"/>)
    }
    return (
        <>
            {userInfoStatus &&
                <>
                    <ul className="list-group">
                        <li className="list-group-item">Iм'я: {userInfoObject.firstname}</li>
                        <li className="list-group-item">Призвіще: {userInfoObject.lastname}</li>
                        <li className="list-group-item">Вік: {userInfoObject.age}</li>
                        <li className="list-group-item">Адреса доставки: {userInfoObject.deliveryAddress}</li>
                        <li className="list-group-item">Номер телефону: {userInfoObject.phoneNumber}</li>
                    </ul>
                    <button className="btn btn-primary"
                            onClick={() => dispatch(userInfoReducer.removeUserInfo())}>Remove information
                    </button>
                </>
            }
        </>
    )
}