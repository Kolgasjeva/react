
import Store from "../store"

import Header from "../components/Header";
import ProductsPages from "../pages/ProductsPages";
import FavorProductsPages from "../pages/FavorProductPages";
import CartPages from "../pages/CartPages"
import {BrowserRouter, useRoutes, Navigate} from "react-router-dom";
import FormPages from "../pages/FormPages";
import UserInfoPages from "../pages/userInfoPages";

function Routes() {
    return (
        useRoutes([
            {
                path: '/',
                element: <Header />,
                children: [
                    {
                        path: '',
                        element: <Navigate to="productlist/"/>,
                    },
                    {
                        path: 'productlist/',
                        element: <ProductsPages/>


                    },
                    {
                        path: 'productlist/favor/',
                        element: <FavorProductsPages/>
                    },
                    {
                        path: 'productlist/cart/',
                        element: <CartPages/>
                    },
                    {
                        path: 'productlist/form/',
                        element: <FormPages/>
                    },
                    {
                        path: 'productlist/userinfo/',
                        element: <UserInfoPages />
                    },
                ]
            }
        ])
    )
}

export default function App() {
    return (
        <>
            <Store >
                <BrowserRouter>
                    <Routes/>
                </BrowserRouter>
            </Store>
        </>
    )
}

