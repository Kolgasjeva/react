import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const initialState = {
    products: [],
    status: '',
    favoriteProducts: [], //store product articles
    cartProducts: [], //store product articles
};

export const fetchAsync = createAsyncThunk(
    'loadReducer/fetchAsync',
    async (_, {rejectWithValue}) => {
        try {
            const response = await fetch('/products.json')
            return await response.json();
        } catch (e) {
            return rejectWithValue(e.message)
        }
    }
)

const getArticle = (action) => {
    return action && action.payload && action.payload.article || -1;
}

const productsReducer = createSlice({
    name: 'productsReducer',
    initialState,
    reducers: {
        reset(state) {
            state.products = initialState.products;
            state.status = 'cleaned';
        },

        //region Favorites
        addToFavorites(state, action) {
            const article = getArticle(action)

            if (article === -1) return

            const favoriteId = state.favoriteProducts.find(id => id === article);

            // If we already have article in favorites - return
            if (favoriteId !== undefined) return

            state.favoriteProducts = [...state.favoriteProducts, article];
        },
        removeFromFavorites(state, action) {
            const article = getArticle(action)

            if (article === -1) return

            state.favoriteProducts = state.favoriteProducts.filter(id => id !== article);
        },
        //endregion
        //region Cart
        addToCart(state, action) {
            const article = getArticle(action)

            if (article === -1) return

            const cartId = state.cartProducts.find(id => id === article);

            if (cartId !== undefined) return

            state.cartProducts = [...state.cartProducts, article];
        },
        removeFromCart(state, action) {
            const article = getArticle(action)

            if (article === -1) return

            state.cartProducts = state.cartProducts.filter(id => id !== article);
        },
        cleanCart(state, action) {
            state.cartProducts = [];
        }
        //endregion
    },
    extraReducers: {
        [fetchAsync.pending]: (state) => {
            state.status = 'loading';
        },
        [fetchAsync.fulfilled]: (state, action) => {
            state.status = 'loaded';
            state.products = action.payload
        },
        [fetchAsync.rejected]: (state, action) => {
            state.status = 'rejected: error ' + action.payload;
            state.products = [];
        }

    }
});

export const {reset, addToCart, removeFromCart, addToFavorites, removeFromFavorites, cleanCart } = productsReducer.actions;
export default productsReducer.reducer




