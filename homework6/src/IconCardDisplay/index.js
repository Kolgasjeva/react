import React from 'react';

export default function IconCardDisplay () {
    return (<svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
    >
        <path d="M0 0h24v24H0z" fill="none" />
        <path
            d="M4 5v14a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V5a2 2 0 0 0-2-2H6a2 2 0 0 0-2 2zm2 0h10v14H6V5z"
            fill="rgba(0, 0, 0, 0.87)"
        />
    </svg>)
}
