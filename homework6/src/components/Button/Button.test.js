import ReactDOM from 'react-dom/client';
import Button from "./index";

import {act} from 'react-dom/test-utils'

import {fireEvent} from '@testing-library/react';

global.IS_REACT_ACT_ENVIRONMENT = true;

let container = null;
let root = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

describe('testing Button Component', () => {
    test('test the absence of default props', () => {
        act(() => {
            root.render(<Button text='Оk'
                                key='ok'/>);
        });
        const element = document.querySelector('button');
        expect(element).toBeTruthy();
        expect(element.style.backgroundColor).toBe('rgb(255, 255, 255)')

    })
    test('call onClick with the correct arguments on click', () => {
        const removeFromCartMock = jest.fn();
        const someArticle = '123456'
        act(() => {
            root.render(<Button backgroundColor='#62d44d'
                                text='Оk'
                                onClick={() => removeFromCartMock(someArticle)}
                                key='ok'/>);
        });
        const element = document.querySelector('button');
        fireEvent.click(element);
        expect(removeFromCartMock).toHaveBeenCalledTimes(1);
        expect(removeFromCartMock).toHaveBeenCalledWith(someArticle);
    })
    test('snapShot test Button Component', () => {
        act(() => {
            root.render(<Button backgroundColor='#62d44d'
                                text='Оk'
                                onClick={() => removeFromCartMock(someArticle)}
                                key='ok'/>);
        });
        const element = document.querySelector('button');
        expect(element.outerHTML).toMatchSnapshot();
    })
})