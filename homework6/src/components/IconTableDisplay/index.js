import React from 'react';

export default function IconTableDisplay () {
    return (<svg
        xmlns="http://www.w3.org/2000/svg"
        width="40"
        height="40"
        viewBox="0 0 24 24"
    >
        <path d="M0 0h24v24H0z" fill="none" />
        <path
            d="M5 18h14V6H5v12zm2-8h10v2H7V10z"
            fill="rgba(0, 0, 0, 0.87)"
        />
    </svg>)
}
