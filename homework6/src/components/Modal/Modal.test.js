import ReactDOM from 'react-dom/client';
import Modal from "./index";
import '@testing-library/jest-dom/extend-expect';
import {act} from 'react-dom/test-utils';

import {render, fireEvent} from '@testing-library/react';

global.IS_REACT_ACT_ENVIRONMENT = true;

let container = null;
let root = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.append(container);
    root = ReactDOM.createRoot(container);
})

afterEach(() => {
    container.remove();
    container = null;
    root = null;
});

describe('testing Modal Component', () => {
    test('test renders modal header and text', () => {
        const { getByText } = render(<Modal header='Confirm adding'
                               text='Do you want to add a product to your cart?'/>);

        const headerElement = getByText('Confirm adding');
        const textElement = getByText('Do you want to add a product to your cart?');

        expect(headerElement).toBeInTheDocument();
        expect(textElement).toBeInTheDocument();
    })
    test('test onClick when modal-overlay is clicked', () => {
        const onClickMock = jest.fn();

        act(()=> root.render(<Modal header='Confirm adding'
                                    closeButton={true}
                                    text='Do you want to add a product to your cart?'
                                    onClick={()=> onClickMock()}
        />))

       const modalOverlay = document.querySelector('.modal-overlay')

        fireEvent.click(modalOverlay);
        expect(onClickMock).toHaveBeenCalled()
    })
    test('test onClick when btn__close-modal is clicked', () => {
        const onClickMock = jest.fn();

        act(()=> root.render(<Modal header='Confirm adding'
                                    closeButton={true}
                                    text='Do you want to add a product to your cart?'
                                    onClick={()=> onClickMock()}
        />));

        const btnClose = document.querySelector('.btn__close-modal');

        fireEvent.click(btnClose);
        expect(onClickMock).toHaveBeenCalled();
    })
    test('snapShot test Button Component', () => {
        act(()=> root.render(<Modal header='Confirm adding'
                                    closeButton={true}
                                    text='Do you want to add a product to your cart?'
                                    onClick={()=> onClickMock()}
        />));
        const element = document.querySelector('.modal-overlay');
        expect(element).toMatchSnapshot();
    })

})