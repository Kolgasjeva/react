import React, {useEffect} from 'react';
import './scss/modal.scss'

function Modal(props) {

    useEffect(() => {
        const modal = document.querySelector('.modal-overlay')
        modal.addEventListener('click', closeModal)
    })
    const closeModal = (event) => {
        if (event.target.classList.contains('modal-overlay') || event.target.classList.contains('btn__close-modal')) {
            props.onClick();
        }
    }
    return (
        <div className='modal-overlay' data-testid="modal-overlay">
            <div className='modal-comtainer' style={{backgroundColor: props.backgroundColor}}>
                <h2>{props.header}</h2>
                {props.closeButton &&
                    <span className='btn__close-modal' onClick={props.onClick}>&times;</span>
                }
                <p>{props.text}</p>
                <div className="modal-actions">{props.actions}</div>
            </div>
        </div>
    );
}

export default Modal