import React, {useState}  from 'react';
import Icon from "../img";
import '../Modal/scss/modal.scss'
import {useSelector, useDispatch} from "react-redux";

import * as modalReducer from "../../store/reducers/modalReducer";
import * as productsReducer from "../../store/reducers/productsReducer";

export default function ProductItem(props) {
    const products = useSelector(state => state.productsReducer.products)
    const favorites = useSelector(state => state.productsReducer.favoriteProducts)
    const cart = useSelector(state => state.productsReducer.cartProducts)

    const dispatch = useDispatch();

    const [viewDetails, setViewDetails] = useState(false)

    const product = products.find(item => item.article === props.product.article)

    const getArticle = () => {
        return props.product.article
    }
    return (
        <>
            <li className="products__item-table">
                <img src={'/img/' + props.product.url} alt="card" className="products__img"/>
                {favorites.includes(getArticle()) ?
                    <span className="icon-favorites"
                          onClick={() => dispatch(productsReducer.removeFromFavorites({article: props.product.article}))}>
                        <Icon color="gold"/>
                        </span> :
                    <span className="icon-favorites"
                          onClick={() => dispatch(productsReducer.addToFavorites({article: props.product.article}))}>
                         <Icon color="#bdbebd"/>
                        </span>
                }
                <h5 className="products__title-table">{props.product.title}</h5>
                {viewDetails === false && <span className="btn__detais-product" onClick={() => setViewDetails(true)}>Show details</span>}
                {viewDetails === true &&
                    <>
                        <span  className="btn__detais-product" onClick={() => setViewDetails(false)}>Hide details</span>
                        <p className="details">
                            <span className="products__price">Ціна: {props.product.price} грн</span>
                            <span className="products__info--item">Артикул: {props.product.article}</span>
                            <span className="products__info--item">Колір: {props.product.color}</span>
                        </p>
                    </>
                }
                {
                    cart.includes(getArticle()) ?
                        <>
                            <button
                                className="btn__remove btn__close-modal"
                                onClick={() => dispatch(modalReducer.toggleRemoveFromCartModal({article: props.product.article}))}
                            >&times;</button>
                            <p className="status__in-cart">Item in cart</p>
                        </> :
                        <button className="btn__add"
                                onClick={() => dispatch(modalReducer.toggleAddToCartModal({article: props.product.article}))}>Add
                            to cart</button>
                }
            </li>
        </>
    )
}