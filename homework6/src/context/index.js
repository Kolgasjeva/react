import  {createContext, useState} from "react";

export const displayProductsContext = createContext()

export default function DisplayProductsContextContainer ({children}) {
    const [displayProducts, setDisplayProducts] = useState('card')
    return (
        <displayProductsContext.Provider value={{displayProducts, setDisplayProducts}}>
            {children}
        </displayProductsContext.Provider>
    )
}