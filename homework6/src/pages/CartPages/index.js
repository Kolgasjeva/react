import {useSelector, useDispatch} from "react-redux";
import ProductsList from "../../components/ProductsList";
import * as productsReducer from "../../store/reducers/productsReducer";
import {Navigate} from "react-router-dom";
import React from "react";
import { useNavigate } from 'react-router-dom';


export default function CartPages(props) {
    const cart = useSelector(state => state.productsReducer.cartProducts)
    const products = useSelector(state => state.productsReducer.products)
    const userInfoStatus = useSelector(state => state.userInfoReducer.userInfoStatus);
    const userInfoObject = useSelector(state => state.userInfoReducer.userInfoObject);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function getProductsInCart() {
        return products.filter(product => cart.includes(product.article))
    }

    function buyProducts() {
        if (!userInfoStatus) {
            navigate('/productlist/form/');
        }
        if (userInfoStatus) {
            console.log('Iнформація про користувача')
            Object.keys(userInfoObject).forEach(key => {
                console.log(`${key}: ${userInfoObject[key]}`)
            })
            const productInCard = products.filter(product => cart.includes(product.article));
            console.log('Iнформація про придбані товари')
            productInCard.forEach(product => {
                Object.keys(product).forEach(key => {
                    console.log(`${key}: ${product[key]}`)
                })
            })
            dispatch(productsReducer.cleanCart())
        }
    }

    return (
        <>
            {cart.length === 0 ? <div className="no-items">No items in cart :(</div> :
                <ProductsList products={getProductsInCart()}></ProductsList>}
            {cart.length>0 && <button className="btn btn-primary" onClick={buyProducts}>Checkout</button>}
        </>
    )
}