import modalReducer, { toggleAddToCartModal, toggleRemoveFromCartModal, initialState } from './modalReducer';

describe('testing modalReducer', () => {
    test('Should return the init state', () => {
        const unknownAction = { type: 'UNKNOWN_ACTION' };
        expect(modalReducer(initialState, unknownAction)).toEqual(initialState);
    })

    test('Should handle toggleAddToCartModal', () => {
        const nextState = modalReducer(initialState, toggleAddToCartModal({ article: '123456' }));
        expect(nextState.addToCartModal).toEqual('123456');
    })

    test('Should handle removeFromCartModal', () => {
        const nextState = modalReducer(initialState, toggleRemoveFromCartModal({ article: '123456'}));
        expect(nextState.removeFromCartModal).toEqual('123456');
    })
});