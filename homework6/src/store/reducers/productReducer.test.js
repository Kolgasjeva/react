import productsReducer, {
    addToFavorites,
    removeFromFavorites,
    addToCart,
    removeFromCart,
    cleanCart,
    initialState
} from './productsReducer';
import {fetchAsync} from "./productsReducer";



const products = [
    {
        title: "Жіночий рюкзак Sambag",
        price: 2719,
        url: "11321008-800x1108.jpg",
        article: "123456",
        color: "білий"
    },
    {
        title: "Жіночий рюкзак Sambag Zard LST",
        price: 3300,
        url: "25018001-800x1108.jpg",
        article: "546712",
        color: "чорний"
    },
]

describe('testing productReducer', () => {

    test('addToFavorites should add an article to favoriteProductsм', () => {
        const initialState = {favoriteProducts: []};
        const nextState = productsReducer(initialState, addToFavorites({article: '123456'}));
        expect(nextState.favoriteProducts).toEqual(['123456'])
    })

    test('removeFromFavorites should remove an article from favoriteProducts', () => {
        const initialState = {favoriteProducts: ['123456', '234567']};
        const nextState = productsReducer(initialState, removeFromFavorites({article: '123456'}));
        expect(nextState.favoriteProducts).toEqual(['234567'])
    })

    test('addToCart should add an article to cartProducts', () => {
        const initialState = {cartProducts: []};
        const nextState = productsReducer(initialState, addToCart({article: '123456'}));
        expect(nextState.cartProducts).toEqual(['123456'])
    })

    test('removeFromCart should remove an article from cartProducts', () => {
        const initialState = {cartProducts: ['123456', '234567']};
        const nextState = productsReducer(initialState, removeFromCart({article: '123456'}));
        expect(nextState.cartProducts).toEqual(['234567'])
    })

    test('cleanCart should empty the cartProducts array', () => {
        const initialState = {cartProducts: ['123456', '234567']};
        const nextState = productsReducer(initialState, cleanCart());
        expect(nextState.cartProducts).toEqual([])
    })

    test('fetchAsync.pending should set status to loading', () => {
        const initialState = { status: '' };
        const nextState = productsReducer(initialState, fetchAsync.pending);
        expect(nextState.status).toBe('loading');
    })

    test('fetchAsync.fulfilled should set status to loaded and update products', () => {
        const initialState = { status: 'loading', products: [] };
        const nextState = productsReducer(initialState, fetchAsync.fulfilled(products));
        expect(nextState.status).toBe('loaded');
        expect(nextState.products).toEqual(products);
    })
})