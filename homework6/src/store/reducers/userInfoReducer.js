import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";

const initialState = {
    userInfoObject: {},
    userInfoStatus: false,
};

const userInfoReducer = createSlice({
    name: 'userInfoReducer',
    initialState,
    reducers: {
        addUserInfo(state, action) {
            state.userInfoStatus = true;
            console.log(action.payload.userInfo)
            state.userInfoObject = action.payload.userInfo;
        },
        removeUserInfo(state) {
            state.userInfoStatus = false;
            state.userInfoObject = {};
        },
    }
})
export const {addUserInfo, removeUserInfo} = userInfoReducer.actions;
export default userInfoReducer.reducer
